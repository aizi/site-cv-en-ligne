<!DOCTYPE html>
<html>
<head>
    <title>Aizi Sarra</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
  
  <?php
  // message de confirmation
  if (isset($success)) { 
      echo "<div class=\"success\">$success</div>";
  }
  // message d'erreur
  if (isset($error)) { 
      echo "<div class=\"error\">$error</div>";
} ?>


<div class="page">
        <h2 div class="titre">Bienvenue sur notre site</h2>
        <article>
          <nav div class="sommaire">
            <ul>
              <li><a href="index.php">Retour au Menu </li>
          </ul>
          </article>
        
 </div>
</body>
</html>