<?php         session_start(); ?>
<!DOCTYPE html>
<head>
    <title>Aizi Sarra</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
<?php
        
        // afficher le message d'erreur (s'il y a)
        if (isset($_SESSION['error'])) {
            echo '<div class="error">'.$_SESSION['error'].'</div>';
        }

        // afficher le message de succès (s'il y a)
        if (isset($_SESSION['success'])) {
            echo '<div class="success">'.$_SESSION['success'].'</div>';
        }

        // on vide les messages d'erreur pour qu'ils ne s'affichent qu'une seule fois
        unset($_SESSION['error']);
        unset($_SESSION['success']);

        ?>
    <div  class="global">
        <div class="entete" id="1">
            <header>
                    <nav id="nav">
                            <ul>
                              <li><a href="#1">About me</a></li>
                              <li><a href="#2">Compétences</a></li>
                              <li><a href="#3">Projets</a></li>
                              <li><a href="#4">Intérets</a></li>
                              <li><a href="#5">Contact</a></li>
                            </ul>
                    </nav>
                <h1 class="h1-cv">SARRA AIZI</h1>
                <h2 class="h2dev">Développeuse Web</h2>
            </header>
        </div>
        <article>
            <section class="presentation" >
                <h1 class="qui">Qui suis-je?</h1>
                    <p class="pres">Autonome, polyvalente et curieuse, passionnée par l'informatique, les nouvelles technologies,
                    et la création en tout genre sur ordinateur, j’ai démarré cette activité comme un hobby puis j’en ai
                    fait mon métier par passion.</br>En 2017 j'ai décidé de m'investir complètement dans la programmation
                    afin de devenir une développeuse chevronné.
                    </p>
            </section>
            <section class="competences" id="2">
                <div class="compe-titre">
                    <h1 class="comp">Compétences</h1>
                </div>
                <div class="icones-comp">
                    <div class="quatre-comp">
                        <div class="HTML">
                            <div class="HTML-ICONE">
                                <img src="media/html5.png" alt="photo stage" class="hover">
                                    <div class="progressbar" data-perc="75">
                                        <div class="bar color2"><span></span></div>
                                        <div class="label"><span></span></div>
                                    </div>
                            </div>
                        </div>
                        <div class="CSS">
                            <div class="css-icone">
                                <img src="media/css3.png" alt="photo stage" class="hover">
                                <div class="progressbar" data-perc="50">
                                    <div class="bar color3"><span></span></div>
                                    <div class="label"><span></span></div>
                                </div>
                            </div>
                        </div>
                        <div class="javascript">

                            <div class="javascript-icone">
                                <img src="media/js1.png" alt="photo stage" class="hover">
                                <div class="progressbar" data-perc="25">
                                    <div class="bar color4"><span></span></div>
                                    <div class="label"><span></span></div>
                                </div>
                            </div>
                        </div>
                        <div class="php">

                            <div class="php">
                                <img src="media/PHP.png" alt="photo stage" class="hover">
                                <div class="progressbar" data-perc="25">
                                    <div class="bar color4"><span></span></div>
                                    <div class="label"><span></span></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="qua-comp">
                        <div class="wordpress">

                            <div class="wordpress-icone">
                                <img src="media/wp.png" alt="photo stage" class="hover">
                                <div class="progressbar" data-perc="50">
                                    <div class="bar color3"><span></span></div>
                                    <div class="label"><span></span></div>
                                </div>
                            </div>
                        </div>
                        <div class="scrum">
                            <div class="scrum-icone">
                                <img src="media/scrum.png" style="height:127px; " alt="photo stage" class="hover">
                                <div class="progressbar" data-perc="50">
                                    <div class="bar color3"><span></span></div>
                                    <div class="label"><span></span></div>
                                </div>
                            </div>
                        </div>
                        <div class="gitlab">

                            <div class="gitlab-icone">
                                <img src="media/git1.png" alt="photo stage" class="hover">
                                <div class="progressbar" data-perc="100">
                                    <div class="bar"><span></span></div>
                                    <div class="label"><span></span></div>
                                </div>
                            </div>
                        </div>
                        <div class="mysql">

                            <div class="mysql-icone">
                                <img src="media/mysql.png" alt="photo stage" class="hover">
                                <div class="progressbar" data-perc="50">
                                    <div class="bar color3"><span></span></div>
                                    <div class="label"><span></span></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section class="porte-fulio" id="3">
                        <div class="t-porte">
                            <h1 class="por">Projets</h1>
                        </div>
                <div class="mes-projets">
                    <div class="jeux tuile site"><a href="reproduction-google-drive/index.html"><span style="font-size: 83px;
                        color: transparent">google</span></a></div>
                    <div class="jeux tuile jeux-v"><a href="kaerujump-jeux-video/jeux.html"><span style="font-size: 83px;
                        color: transparent">jeuxvideo</span></a></div>
                    <div class="jeux tuile puissance"><a href="puissance4DV/puissance-acc.html"><span style="font-size: 83px;
                        color: transparent">puissance4</span></a></div>
                    <div class="todo-list tuile todo"><a href="todolist/index.php"><span style="font-size: 83px;
                        color: transparent">todolist</span></a></div>
                    <div class="site-google tuile playlist"><a href="playlist/connexion.php"><span style="font-size: 83px;
                        color: transparent">playlist</span></a></div>
                </div>
            </section>
            <section class="interets" id="4">
                <div class="t-int">
                    <h1 class="inte">Interets</h1>
                </div>
                <div class="int">
                    <div class="technologie">
                        <h1 class="tech">Technologie</h1>
                        <img src="media/technologie.png" alt="photo stage" class="hover">
                    </div>
                    <div class="cuisine">
                        <h1 class="cui">Cuisine</h1>
                        <img src="media/cuisine.png" alt="photo stage" class="hover">
                    </div>
                    <div class="cinema">
                        <h1 class="cin">Cinema</h1>
                        <img src="media/cinema.png" alt="photo stage" class="hover">
                    </div>
                </div>
            </section>
        </article>
        <footer class="footer" id="5">
            <div class="contact">
                <h1 class="cont">Envie de me contacter ?</h1>
            </div>
            <div class="footer-from-contact">
                <div class="contact-i">
                    <div class="pdf-mail">
                        </br>
                        <div class="pdf-cv">
                            <img src="media/pdf.png" alt="photo stage">
                            <a href="mon-cv.pdf" class="lien" id="lien-a">Telecharger mon CV</a>
                        </div>
                        <div class="email">
                            <img src="media/mail.png" alt="photo stage">
                            <p>aizi-sarra@live.fr</p>
                        </div>
                        <div class="tel">
                            <img src="media/tel.png" alt="photo stage">
                            <p>06 19 22 04 34</p>
                        </div>
                    </div>
                    <div class="icones">
                        <a href="https://www.linkedin.com/in/sarra-aizi-318300155/" id="lien-a"><img src="media/linkedin.png" alt="photo stage"></a>
                        <a href="https://www.skype.com" id="lien-a"><img src="media/skype.png" alt="photo stage"></a>
                        <a href="https://www.gmail.com" id="lien-a"><img src="media/google.png" alt="photo stage"></a>
                        <a href="https://gitlab.com/aizi" id="lien-a"><img src="media/github.png" alt="photo stage"></a>
                        <a href="https://twitter.com/SarraAizi" id="lien-a"><img src="media/twiter.png" alt="photo stage"></a>
                    </div>
                </div>

                <div class="form">
                    
                    <form id="contact_form" action="unpload.php" method="POST">
                        <label class="required" for="name">
                            <span class="votre-">Votre nom</span>
                        </label>
                            <br />
                        <input id="name" class="input" name="nom" type="text" value="" size="30" />
                            <br />

                        <label class="required" for="email">
                            <span class="votre-">Votre email:</span>
                        </label>
                            <br />
                        <input id="email" class="input" name="mail" type="text" value="" size="30" />
                            <br />
                        <label class="required" for="message">
                            <span class="votre-">Votre message:</span>
                        </label>
                            <br />
                        <textarea id="message" class="input" name="message"></textarea>
                            <br />
                        <input id="submit_button" type="submit" value="Envoyer" name="mailform"/>
                    </form>
                </div>
            </div>
                    </br>
                    </br>
            <div class="fin">
                <p id="droit">Site web fait maison Tous droits réservés. </br>© 2018 SARRA AIZI</p>
            </div>
        </footer>
    </div>
    <script src="mon-cv.js"></script>
    <script class="cssdeck" src="//cdnjs.cloudflare.com/ajax/libs/jquery/1.8.0/jquery.min.js"></script>
</body>
</html>