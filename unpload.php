<?php
// configuration SMTP
define('MAIL_SMTP', 'smtp.mailgun.org'); // ex: smtp.mailgun.org
define('MAIL_PORT', 25);
define('MAIL_USERNAME', 'postmaster@sandbox6245716c44e247f4b0373f7147873a5c.mailgun.org');
define('MAIL_PASSWORD', '5c3731df85d2e1b1bf84b4e41122d8ff');


// à qui envoyer l'email
define('CONTACT_FORM_EMAIL', ['aizi-sarra@live.fr' => 'Aizi Sarra']);


if (isset($_POST['submit'])) {
      //recupéré les valeurs du formulaire
    $nom=htmlspecialchars($_POST['nom']);
    $mail=htmlspecialchars($_POST['mail']);
    $message=htmlspecialchars($_POST['message']);

 
/* On vérifie que le format de l'e-mail est correct */ 

// check email 
    if (!preg_match("/[-0-9a-zA-Z.+_]+@[-0-9a-zA-Z.+_]+.[a-zA-Z]{2,4}/",$mail)) {
      $error = "Format d'email invalide"; 
    }
    else {
       // importer SwiftMailer
       require_once 'vendor/autoload.php';


// Create the Transport
        $transport = (new Swift_SmtpTransport(MAIL_SMTP, MAIL_PORT))
        ->setUsername(MAIL_USERNAME)
        ->setPassword(MAIL_PASSWORD)
        ;

// Create the Mailer using your created Transport
        $mailer = new Swift_Mailer($transport);

// Création du message
        $message = (new Swift_Message('Mon Sujet'))
        ->setFrom([$mail => $name])
        ->setTo(CONTACT_FORM_EMAIL)
        ->setBody($message)
        ;


        // Envoi du message
        $result = $mailer->send($message);

        if ($result) {
                $success = 'Message envoyé avec succès';
                header('location:redirection.php');
                // Création du message de confirmation
            $confirmation = (new Swift_Message('Mon Sujet'))
            ->setFrom(CONTACT_FORM_EMAIL)
            ->setTo([$mail => $name])
            ->setBody('<strong>Bonjour</strong><br>Votre message a bien été reçu :)', 'text/html')
            ;

            // Envoi du message
            $confirmationResult = $mailer->send($confirmation);
        }
        else {
            $error = 'Erreur lors de l\'envoi du message';
        }
    }
}


?>
<?php
// fonction qui sauvegarde les messages d'erreur/succès en session
function message($message, $type = 'error') {
  // on démarre la session (si pas déjà fait)
  if (session_status() == PHP_SESSION_NONE) session_start();
  // on vide les messages d'erreur précédent
  unset($_SESSION['error']);
  unset($_SESSION['success']);
  // on enregistre le message
  $_SESSION[$type] = $message;
}


?>